# Voxel

<img src="screenshots/norm.png" width="400">
<img src="screenshots/wire.png" width="400">

Intro
-----
Voxel game engine written in C++ using OpenGL.   
The engine is primarily focused on the challenges behind generating and rendering a big procedural world in real time.

Usage
-----
`./voxel [world_seed]`

### Keymap:  
**WASD**  - move around  
**F**     - toggle fullscreen  
**I**     - toggle debug info HUD  
**M**     - toggle [wireframe](https://gitlab.com/apaget/voxel/master/screenshots/wireframe.png) mode  

Build
-----
```
git clone --recursive https://gitlab.com/apaget/voxel
cd voxel
cmake . && make
```
